A GBT-SCA Low level tool to connect to the SCA OPC UA Server and read various things.

## Deploy

`git clone https://gitlab.cern.ch/ptzanis/scaopcuapyread.git`

## Run

Example reading the SCA internal temperatures of the L1DDC running under SCA OPC UA Server of pc-tdq-flx-nsw-mm-01.cern.ch:48020 :

`python scaopcuapyread.py --mode temp --item internalTemperature --opc pc-tdq-flx-nsw-mm-01.cern.ch:48020 --board L1DDC`


## Contact

Questions, comments, suggestions, or help?

**Polyneikis Tzanis**: <polyneikis.tzanis@cern.ch
