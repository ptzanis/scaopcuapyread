import sys
from ctypes import *

sys.path.append("PyUAF/")
lib = CDLL("PyUAF/libuafutil.so")
lib = CDLL("PyUAF/libuafclient.so")

import time
import pyuaf

from pyuaf.util             import Address, NodeId, opcuaidentifiers
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings, BrowseSettings, SessionSettings
from pyuaf.client.requests  import BrowseRequest, BrowseRequestTarget
from pyuaf.util             import loglevels, opcuastatuscodes
from pyuaf.util             import Address, NodeId
from pyuaf.util             import primitives
from pyuaf.util             import opcuaidentifiers
from pyuaf.util.errors      import UafError

client = []

def connect():
	global client
	cs = ClientSettings("myClient", [server_address])
# Uncomment below for session logging
	# cs.logToStdOutLevel = loglevels.Info 
	max_attempts=40
	num_attempts=0
	while (True):
		print("Trying to connect to the OPC UA Server. Attempt #"+str(num_attempts))
		result=None
		try:
			client = Client(cs)
			rootNode = Address( NodeId(opcuaidentifiers.OpcUaId_RootFolder, 0), server_uri )
			result=client.browse ([ rootNode ])
		except Exception as e:
			print("Got exception: "+str(e))
			num_attempts = num_attempts+1
			if num_attempts > max_attempts:
				msg = 'Despite '+str(max_attempts)+ ' attempts couldnt establish OPC UA connection: exception is '+str(e)
				print(msg)
				dcs_test_utils.log_detail(False,msg,'')
				raise msg
			time.sleep(3)
			continue
		print('Connected to OPC UA Server')
		return
			
def browseOpcAndReturnBoardList():
    rootNode = Address( NodeId(opcuaidentifiers.OpcUaId_RootFolder, 0), server_uri )
    cs = ClientSettings("myClient", [server_address])
    myClient = Client(cs)
    firstLevelBrowseResult = myClient.browse([rootNode])
    firstLevelBrowseRequest = BrowseRequest(1)
    firstLevelBrowseRequest.targets[0].address = rootNode
    firstLevelBrowseResult = myClient.processRequest(firstLevelBrowseRequest)
    
    assert len(firstLevelBrowseResult.targets[0].continuationPoint) == 0
    
    noOfFoundReferences = len(firstLevelBrowseResult.targets[0].references)
    secondLevelBrowseRequest = BrowseRequest(noOfFoundReferences)
    for i in xrange(noOfFoundReferences):
        secondLevelBrowseRequest.targets[i].address = Address(firstLevelBrowseResult.targets[0].references[i].nodeId)
    
    secondLevelBrowseResult =  myClient.processRequest(secondLevelBrowseRequest)

    forbiddenList = ['FolderType','Server','StandardMetaData','Meta','theGlobalStatistician','theSupervisor']

    boardList = []

    for i in xrange(len(secondLevelBrowseResult.targets[2].references)):
    	boardName = str(secondLevelBrowseResult.targets[2].references[i].displayName)
    	if boardName not in forbiddenList:
    	   boardList.append(boardName)

    return boardList



def addr(sa):
    return Address(NodeId(sa,server_ns), server_uri)

def addr_to_str(address):
	return address.getExpandedNodeId().nodeId().identifier().idString

def map_errors(targets):
        return ' '.join(map (lambda x: str(x.status), targets))

def read(addresses):
        rv = client.read(addresses)
        if not rv.overallStatus.isGood():
                raise Exception ("read failed with the following error code: "+str(rv.overallStatus)+" specific error code(s):"+map_errors(rv.targets) )
        return rv

def write(addresses, values):
        rv = client.write(addresses, values)
        if not rv.overallStatus.isGood():
                raise Exception ("write failed with the following error code: "+str(rv.overallStatus)+" specific error code(s):"+map_errors(rv.targets))
        return rv
