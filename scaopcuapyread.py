#!/usr/bin/python

import time
import re
import sys
sys.path.insert(1, 'src/')
import argparse
from argparse import RawTextHelpFormatter
import os
import dcs_opc_ua

# ----------------------  Function arguments -------------------------------------------------------

parser = argparse.ArgumentParser(description=' *** ScaOpcUaPyRead Tool to do various things through the SCA OPC UA Server ***', formatter_class=RawTextHelpFormatter)
parser.add_argument("-mode", "--mode", type=str,
   help = "Choose mode: sca | temp | pw | value")
parser.add_argument("-item", "--item", type=str,
   help = "Choose item: \n"
        "  SCA options: id ,address, online \n"
        "  ADDC options: gbtx[0-1]NTC/boardTemp/feastTemp[1-2]v5/internalTemperature/[1-2]v5TP[0-1] \n"
        "  L1DDC options: GBTX[1-3]_TEMP/[1-2]V5_PTAT/internalTemperature/P[1-2]V5_SCA/VTRX[1-2]_RSSI \n"
        "  MMFE8 options: vmmPdo[0-7]/pTat1V3A/pTat1V3AE/pTat1V2D/internalTemperature/0V65for1V3A/0V65for1V3E/0V6for1V2D \n"
        "  SFEB options: vmmPdo[0-7]/internalTemperature \n"
        "  PFEB options: vmmPdo[0-2]/internalTemperature \n"
        )
parser.add_argument("-opc", "--opc", type=str,
   help = "Insert SCA OPC UA Server Host PC, example: pcatlnswfelix04:48020")
parser.add_argument("-board", "--board", type=str,
   help = "Insert selected board type: MMFE8 | ADDC | L1DDC | SFEB | PFEB")
parser.add_argument("-sector", "--sector", default="A00", type=str,
   help = "Selected boards")
parser.add_argument("-save", "--save", action='store_true',
   help = "Save output on text file (mandatory to put sector id using -sector A00 and -path "" )")
parser.add_argument("-path", "--path", type=str, default="",
   help = "Path to save data")
args = vars(parser.parse_args())

# ----------------------  Function arguments error handling -------------------------------------------------------

# Select Sector
mode = args['mode']

# Select item
item = args['item']

# Select FELIX computer
opc = args['opc']

# Select sector
sector = args['sector']

# Select boardType

selectedBoardType = args['board']
selectedBoardType = selectedBoardType.split(',')

# Save data
save = args['save']

# Save data path
path = args['path']

# ----------------------  Connection settings -------------------------------------------------------

dcs_opc_ua.server_uri = 'urn:CERN:OpcUaScaServer'
dcs_opc_ua.server_ns = 2
scaOpcAddress = 'opc.tcp://'+opc
dcs_opc_ua.server_address = scaOpcAddress
dcs_opc_ua.connect()

# ----------------------  Browse boards  -------------------------------------------------------

boardList = dcs_opc_ua.browseOpcAndReturnBoardList()

# ----------------------  Selected boards  -------------------------------------------------------

selectedBoardList = []

for board in boardList:
    for boardType in selectedBoardType:
       if boardType in board:
           selectedBoardList.append(board)

# ----------------------  Functions  -------------------------------------------------------

def printBoardItemAndValue(boardList, item):
    if(mode=="sca"):
        bareItem = item.split('.') [1]
    else:
        bareItem = item.split('.') [2]

    if(save):
        dataFile = open(path+'data_'+sector+'_'+mode+'_'+bareItem+'_'+str(selectedBoardType[0])+'.txt','w+')

    for i,board in enumerate(boardList):
            if(board == "RimL1DDC_PRI" and item ==".ai.internalTemperature.temperature"):
                item = ".ai.internalTemperature1.temperature"
            if(board == "RimL1DDC_AUX" and item ==".ai.internalTemperature.temperature"):
                item = ".ai.internalTemperature2.temperature"
            boardItem = board+item
            opcua_addr = dcs_opc_ua.addr(boardItem)
            value = dcs_opc_ua.read ([opcua_addr])
            print(i+1, board, bareItem, str(value.targets[0].data))
            if(save):
                dataFile.write("{} {}\n".format(board, str(value.targets[0].data)))

    if(save):
        dataFile.close()

# ----------------------  Run  -------------------------------------------------------

if(mode == "sca"):
	itemToRead = '.' + item
if(mode == "temp"):
	itemToRead = '.ai.' + item +'.temperature'
if(mode == "pw"):
    itemToRead = '.ai.' + item +'.power'
if(mode == "value"):
    itemToRead = '.ai.' + item +'.value'

printBoardItemAndValue(selectedBoardList, itemToRead)
